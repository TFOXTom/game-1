function love.load()

	sWidth = 640
	sHeight = 480

	love.window.setMode(sWidth, sHeight, flags)
	playerSprite = love.graphics.newImage("player.png")
	love.graphics.setBackgroundColor(255, 000, 150)

	x = 100
	y = 100
	speed = 1000

end

function love.update(dt)

	if love.keyboard.isDown("w") and y >= 0 then

		y = y - (speed * dt)

	end

	if love.keyboard.isDown("s") and y <=  sHeight - 64 then

		y = y + (speed * dt)

	end

	if love.keyboard.isDown("a") and x >= 0 then

		x = x - (speed * dt)

	end

	if love.keyboard.isDown("d") and x <= sWidth - 64 then

		x = x + (speed * dt)

	end

end

function love.draw()
	
	love.graphics.draw(playerSprite, x, y)

end